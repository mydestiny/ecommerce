<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'secret');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'I<B@:g$|uuTG/A{b0Lj,@_K#%qXF@Hq[y2,(v2?kK!<P4MoHUi9+Cm23<6{9WkT}');
define('SECURE_AUTH_KEY',  '^bXF@.-JlS-P.8$Psfl-{63(SY]co>Mh?iJ5_t@cmx|6n.4{Y`>{%sMn*:9VQuV}');
define('LOGGED_IN_KEY',    '}iF0^lc:Q^0`XA)$j6_}Ut{roEI% T-d<9q)BZlV=.@VkJq&U %fK7cEor~~K ,b');
define('NONCE_KEY',        '_?dlF5 wP`%bci|3!r<abA-:^ usfRSvJ%^owNu,932aZ9V/EmU}@8xP[v88(RjD');
define('AUTH_SALT',        '}fdl9s,@kn7r&!OKbjX]pEaY{wwE|E[wzotMt3v oTI]Y5;M^l$D90k|2_Jfdif_');
define('SECURE_AUTH_SALT', 'm#967!+!.:`2EP(kl=+=k?q2|*jY|LjO=pX.^>JS|D?5*MjMB^=0BlE>GwKdm}PN');
define('LOGGED_IN_SALT',   'e|yG+[i?RYR*XCrEN3kk_`XPAp,a:Zm=,JFfNRuZ{mG}yehoMK:_bS,GJR8wr:T&');
define('NONCE_SALT',       'xKw?SOLtslbfoyz]nj24}U{LVZA9LZKNHn<Q(7&!&BaelfkVk9^iUKkN11NC.}%t');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
